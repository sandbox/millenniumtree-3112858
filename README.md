# About

Commerce Snapshot creates SKU and Variation snapshot fields on the Order Item entity,
and records this Variation data at the time of checkout, in case
the product or variation are removed after the order is placed.

# Installation

Commerce Snapshot depends on the JSON Field module found at https://www.drupal.org/project/json_field

Once installed, navigate to Commerce -> Configuration -> Orders -> Order item types.
Edit each order item type, and check the 'Snapshottable' checkbox.
This will add two new fields to the order item type.
* SKU Snapshot (Stores a plain-text SKU from the purchased variation at checkout)
* Variation Snapshot (Stores a JSON-serialized copy of the entire purchased variation)

Existing order items that still have a variation will be snapshotted the next time the order is loaded.
New order items will be snapshotted at checkout time, so the SKU and item details can be examined
later, even after the variation and product have been deleted.
