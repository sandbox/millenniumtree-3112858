<?php

namespace Drupal\commerce_snapshot\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_order\Event\OrderItemEvent;

class OrderItemSnapshot implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      OrderEvents::ORDER_ITEM_PRESAVE => 'presaveOrderItem',
      OrderEvents::ORDER_ITEM_LOAD => 'loadOrderItem',
    ];
    return $events;
  }

  /**
   *
   * Fire on order item presave
   * @param \Drupal\commerce_order\Event\OrderItemEvent $event
   *   The presave event
   */
  public function presaveOrderItem(OrderItemEvent $event) {
    $order_item = $event->getOrderItem();
    $order = $order_item->getOrder();

    // If a variation exists for this order item...
    if (!empty($order) && !in_array($order->getState()->value, ['canceled'])) {
      $order_item = $this->takeSnapshot($order_item);
    }
  }

  /**
   *
   * Fire on order item load
   * @param \Drupal\commerce_order\Event\OrderItemEvent $event
   *   The load event
   */
  public function loadOrderItem(OrderItemEvent $event) {
    static $saving = []; // This will prevent an infinite loop by not attempting to save the same order item twice

    $order_item = $event->getOrderItem();
    $order = $order_item->getOrder();

    // If a variation exists for this order item...
    if (!empty($order) && !in_array($order->getState()->value, ['canceled'])) {
      if(!isset($saving[$order_item->id()])) {
        $saving[$order_item->id()] = TRUE;
        $modifiedEntity = $this->takeSnapshot($order_item);
        if(!empty($modifiedEntity)) $modifiedEntity->save();
      }
    }
  }

  private function takeSnapshot($order_item) {
    $modified = FALSE;
    $variation = $order_item->getPurchasedEntity();

    if(!empty($variation) && method_exists($variation, 'getSku')) {
      if($order_item->hasField('sku_snapshot')) {
        if($variation->getSku() != $order_item->get('sku_snapshot')->value) {
          $order_item->set('sku_snapshot', $variation->getSku());
          $modified = TRUE;
        }
      }
      if($order_item->hasField('variation_snapshot')) {
        $json = \Drupal::service('serializer')->serialize($variation, 'json');
        // Remove uuids from the JSON, otherwise, deleted entities referred to here could cause a crash
        $json = preg_replace('/,"uuid":\[\{"value":"[^"]+"\}\]/', '', $json);
        $json = preg_replace('/,"target_uuid":"[^"]+"/', '', $json);
        $order = $order_item->getOrder();

        if(
          empty($order_item->get('variation_snapshot')->value)
          || (
            !in_array($order->getState()->value, ['canceled', 'completed'])
            && $json != $order_item->get('variation_snapshot')->value
          )
        ) {
          $order_item->set('variation_snapshot', $json);
          $modified = TRUE;
        }
      }
    }
    return $modified? $order_item: NULL;
  }
}
