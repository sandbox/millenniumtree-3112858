<?php

namespace Drupal\commerce_snapshot\Plugin\Commerce\EntityTrait;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;

/**
 * Provides the "order_item_snapshottable" trait.
 *
 * @CommerceEntityTrait(
 *   id = "order_item_snapshottable",
 *   label = @Translation("Snapshottable"),
 *   entity_types = {"commerce_order_item"}
 * )
 */
class OrderItemSnapshottable extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['sku_snapshot'] = BundleFieldDefinition::create('string')
      ->setLabel(t('SKU Snapshot'))
      ->setDescription(t('Saved snapshot of the item SKU at time of checkout.'))
      ->setRequired(FALSE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);
    $fields['variation_snapshot'] = BundleFieldDefinition::create('json')
      ->setLabel(t('Variation Snapshot'))
      ->setDescription(t('Saved snapshot of the purchased Product Variation.'))
      ->setRequired(FALSE)
      ->setSettings([
        'size' => '16777216',
      ]);

    return $fields;
  }

}
