<?php

namespace Drupal\commerce_snapshot\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\commerce_product\Entity\ProductVariation;

/**
 * Plugin implementation of the 'product_variation_snapshot' formatter.
 *
 * @FieldFormatter(
 *   id = "snapshot_product_variation",
 *   label = @Translation("Product Variation Snapshot"),
 *   field_types = {
 *     "json",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class SnapshotProductVariation extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $summary = '';
    foreach ($items as $delta => $item) {
      $productVariation = \Drupal::service('serializer')->deserialize($item->value, \Drupal\commerce_product\Entity\ProductVariation::class, 'json');
      // Display some basic data about the variation
      $elements[$delta] = [
        '#markup' =>
          ($productVariation->getTitle()? $this->t('Title').': '.$productVariation->getTitle()."<br/>\n":'').
          ($productVariation->getSku()? $this->t('SKU').': '.$productVariation->getSku()."<br/>\n":'')
        ,
      ];
    }

    return $elements;
  }

}
